import React, { useState } from "react";
import api from "../../services/api";

export default function Login({ history }) {
	//desestutura o vetor que retorna
	const [email, setEmail] = useState("");

	async function handleSubmit(event) {
		event.preventDefault();

		// document.querySelector("input#email");
		const response = await api.post("/sessions", { email });

		const { _id } = response.data;

		localStorage.setItem("user", _id);

		history.push("/dashboard");
	}

	return (
		<>
			<p>
				Ofereca <strong>spots</strong> para programadores e encontre{" "}
				<strong>talentos</strong> para sua empresa
			</p>

			<form onSubmit={handleSubmit}>
				<label htmlFor="email">E-MAIL *</label>
				<input
					type="email"
					id="email"
					placeholder="Seu Melhor e-mail"
					value={email}
					onChange={event => setEmail(event.target.value)}
				/>
				<button className="btn" type="submit">
					Entrar
				</button>
			</form>
		</>
	);
}
